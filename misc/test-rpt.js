const host = 'http://localhost:8080'
const realm = 'kargah'
const publicClientId = 'frontend'
const authzClientId = 'webapi'

const tokenUrl = `${host}/auth/realms/${realm}/protocol/openid-connect/token`

const postToToken = async (body, authorizationToken) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    if(authorizationToken) {
        myHeaders.append("Authorization", `Bearer ${authorizationToken}`);
    }

    var urlencoded = new URLSearchParams();

    for (const key in body) {
        if (Object.hasOwnProperty.call(body, key)) {
            if(Array.isArray(body[key])) {
                for (const value of body[key]) {
                    urlencoded.append(key, value);    
                }
            } else {
                urlencoded.append(key, body[key]);
            }
        }
    }

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };

    const response = await fetch(tokenUrl, requestOptions)
    return await response.json()
}

function decodeToken(str) {
    str = str.split('.')[1];
    str = str.replace(/-/g, '+');
    str = str.replace(/_/g, '/');
    switch (str.length % 4) {
        case 0:
            break;
        case 2:
            str += '==';
            break;
        case 3:
            str += '=';
            break;
        default:
            throw 'Invalid token';
    }
    str = decodeURIComponent(escape(atob(str)));
    str = JSON.parse(str);
    return str;
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(() => resolve(undefined), ms)
    })
}

const waitBetweenSteps = 100;

async function step(step, ...value) {
    console.group(step)
    console.log(...value)
    console.groupEnd()
    await sleep(waitBetweenSteps)
}

(async () => {

    // Step 1: Get Access Token
    const _1 = await postToToken({
        grant_type: 'password',
        username: 'user_1',
        password: 'user_1',
        client_id: 'frontend',
    })
    
    await step(
        'Step 1',
        decodeToken(_1.access_token)
    )

    // Step 2: Update Access Token
    const _2 = await postToToken({
        grant_type: 'refresh_token',
        client_id: 'frontend',
        refresh_token: _1.refresh_token
    })
    await step(
        'Step 2',
        decodeToken(_2.access_token)
    )
    
    // Step 3: Get RPT
    const _3 = await postToToken({
        grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
        audience: 'webapi',
        permission: ['api/test', 'delivery-batches/get_name']
    }, _2.access_token)

    await step(
        'Step 3',
        decodeToken(_3.access_token)
    )

    // Step 4: Update RPT
    const _4 = await postToToken({
        grant_type: 'refresh_token',
        client_id: 'frontend',
        refresh_token: _3.refresh_token
    })
    
    await step(
        'Step 4',
        decodeToken(_4.access_token)
    )

    // Step 4: Update RPT
    const _5 = await postToToken({
        grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
        audience: 'webapi',
        permission: ['api/test']
    }, _4.access_token)

    await step(
        'Step 5',
        decodeToken(_5.access_token)
    )
})()