const fs = require('fs');

console.clear()
const kc = JSON.parse(fs.readFileSync('config.json'))

const lines = []

const scopesIdLookUp = {}
const resourcesIdLookUp = {}
const policiesIdLookUp = {}
const roles = []

for (const {id, name} of kc.scopes) {
    scopesIdLookUp[name] = id
    lines.push(`${id}{{${name}}}`)
}

for (const {name, _id, scopes} of kc.resources) {
    resourcesIdLookUp[name]=_id
    lines.push(`${_id}[(${name})]`)
    
    for (const scope of scopes ?? []) {
        lines.push(`${scopesIdLookUp[scope.name]} --> ${_id}`)
    }
}

for(const {id, name, config} of kc.policies.filter(x => x.type === "role")) {
    lines.push(`${id}([${name}])`)
    policiesIdLookUp[name]=id

    for(const {id: roleId, required} of JSON.parse(config.roles)) {
        if(!roles.includes(roleId)) {
            roles.push(roleId)
            lines.push(`${roleId}(${roleId})`)
        }
        lines.push(`${roleId} ---> ${required ? '|.required.|' : ''} ${id}`)
    }
}

for(const {id, name, config} of kc.policies.filter(x => x.type === "scope")) {

    lines.push(`${id}[[${name}]]`)

    for(const resourceName of JSON.parse(config.resources)) {
        console.log(resourcesIdLookUp)
        lines.push(`${id} -.-> ${resourcesIdLookUp[resourceName]}`)
    }
    for(const scopeName of JSON.parse(config.scopes)) {
        lines.push(`${id} --> ${scopesIdLookUp[scopeName]}`)
    }
    for(const policyName of JSON.parse(config.applyPolicies)) {
        lines.push(`${policiesIdLookUp[policyName]} --> ${id}`)
    }
}


console.log('graph LR\n' + lines.map(x => "\t"+x).join("\n"))