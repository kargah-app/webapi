const host = 'http://localhost:8080'
const realm = 'kargah'
const publicClientId = 'frontend'
const authzClientId = 'webapi'

const tokenUrl = `${host}/auth/realms/${realm}/protocol/openid-connect/token`

const postToToken = async (body, authorizationToken) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    if(authorizationToken) {
        myHeaders.append("Authorization", `Bearer ${authorizationToken}`);
    }

    var urlencoded = new URLSearchParams();

    for (const key in body) {
        if (Object.hasOwnProperty.call(body, key)) {
            if(Array.isArray(body[key])) {
                for (const value of body[key]) {
                    urlencoded.append(key, value);    
                }
            } else {
                urlencoded.append(key, body[key]);
            }
        }
    }

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };

    const response = await fetch(tokenUrl, requestOptions)
    return await response.json()
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(() => resolve(undefined), ms)
    })
}

const waitBetweenSteps = 100;

async function step(step, ...value) {
    console.group(step)
    console.log(...value)
    console.groupEnd()
    await sleep(waitBetweenSteps)
}

(async () => {
    const _1 = await postToToken({
        grant_type: 'password',
        username: 'user_1',
        password: 'user_1',
        client_id: 'frontend',
    })
    
    await step(
        'Step 1',
        _1.access_token,
    )
    
    const _2 = await postToToken({
        grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
        audience: 'webapi',
        permission: ['api/test', 'delivery-batches/get_name']
    }, _1.access_token)

    await step(
        'Step 2',
        _2.access_token,
    )
})()