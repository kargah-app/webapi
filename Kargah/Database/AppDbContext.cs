﻿using System;
using Microsoft.EntityFrameworkCore;
using Kargah.Database.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Kargah.Database {
    public partial class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Company>()
                .HasIndex(b => b.Slug).IsUnique();
        }

        public override async Task<int> SaveChangesAsync(
            CancellationToken cancellationToken = default(CancellationToken)
        ) {
            var createdEntities = (
                from x in this.ChangeTracker.Entries()
                where (
                    x.State == EntityState.Added &&
                    x.Entity != null &&
                    x.Entity as HasCreateUpdateTimestamps != null
                )
                select x.Entity as HasCreateUpdateTimestamps
            );

            var updatedEntities = (
                from x in this.ChangeTracker.Entries()
                where (
                    x.State == EntityState.Modified &&
                    x.Entity != null &&
                    x.Entity as HasCreateUpdateTimestamps != null
                )
                select x.Entity as HasCreateUpdateTimestamps
            );

            var now = (long) DateTime.UtcNow.Subtract(DateTime.UnixEpoch).TotalMilliseconds;
            
            foreach (var e in createdEntities) {
                e.AddedAt = e.ModifiedAt = now;
            }
            
            foreach (var modifiedEntity in updatedEntities) {
                modifiedEntity.ModifiedAt = now;
            }

            return (await base.SaveChangesAsync(true, cancellationToken));
        }

        public virtual DbSet<Company> Companies { get; set; } = default!;
        public virtual DbSet<SpentTime> SpentTimes { get; set; } = default!;
        public virtual DbSet<User> Users { get; set; } = default!;
        public virtual DbSet<Job> Jobs { get; set; } = default!;
    }
}
