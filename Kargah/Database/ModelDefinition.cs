namespace Kargah.Database {
    public interface HasCreateUpdateTimestamps {
        long AddedAt { get; set; }
        long ModifiedAt { get; set; }
    }
}