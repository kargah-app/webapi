using System;

namespace Kargah.Database.Models {
    public class User : HasCreateUpdateTimestamps {
        public Guid UserId {get; set;}

        public Guid CompanyId {get; set;}
        public Company? Company {get; set;}

        public long AddedAt {get; set;}
        public long ModifiedAt {get; set;}
    }
}