using System.ComponentModel.DataAnnotations;
using System;

namespace Kargah.Database.Models {
    public class Company : HasCreateUpdateTimestamps {
        public Guid CompanyId {get; set;}
        
        [MaxLength(64)]
        [MinLength(4)]
        public string Slug {get; set;} = "";

        public long AddedAt {get; set;} = 0;
        public long ModifiedAt {get; set;} = 0;
    }
}