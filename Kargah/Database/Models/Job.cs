using System;
using System.ComponentModel.DataAnnotations;

namespace Kargah.Database.Models {
    public class Job : HasCreateUpdateTimestamps {
        public Guid JobId {get; set;}

        public Guid? ParentJobId {get; set;} = null;
        public Job? ParentJob {get; set;} = null;

        public Guid? CompanyId {get; set;} = null;
        public Company? Company {get; set;} = null;

        [MaxLength(1024)]
        public string Title {get; set;} = "";

        public long AddedAt {get; set;}
        public long ModifiedAt {get; set;}
    }
}