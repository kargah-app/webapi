using System;

namespace Kargah.Database.Models {
    public class SpentTime : HasCreateUpdateTimestamps {
        public Guid SpentTimeId {get; set;}

        public Guid UserId {get; set;}
        public User? User {get; set;} = null;

        public string Description {get; set;} = "";
        
        public long StartedAt {get; set;}
        public long Duration {get; set;}

        public long AddedAt {get; set;}
        public long ModifiedAt {get; set;}
    }
}