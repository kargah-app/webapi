using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using Kargah.Keycloak;

namespace Kargah.GraphQL.Queries {

    public record KeycloakAuthzGetResoourceScopesInput(
        string resourceName
    );

    public partial class _Queries {
        /// <summary>
        /// Get the logged in user permissions for certain resource
        /// </summary>
        [Authorize()]
        public string[]? KeycloakAuthzGetResoourceScopes(
            [Service] IKeycloakAccessToken keycloakAccessToken,
            KeycloakAuthzGetResoourceScopesInput input
        ) {
            return keycloakAccessToken.GetResourceScopes(input.resourceName);
        }
    }   
}