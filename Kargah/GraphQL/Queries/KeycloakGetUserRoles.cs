using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Keycloak;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Queries {

    public partial class _Queries {
        public async Task<IEnumerable<RoleRepresentation>> KeycloakGetUsersRealmRoleMapping(
            [Service] IKeycloakRest kc,
            string userId
        ) {
            return await kc.GetUsersRealmRoleMappingAsync(userId);
        }
    }
}