using System;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Data.Filters;
using HotChocolate.Types;
using Kargah.Database;
using Kargah.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Queries {
    public class CompanyFindByQueryFilterType: FilterInputType<CompanyFindByQueryEntity>
    {
        protected override void Configure(IFilterInputTypeDescriptor<CompanyFindByQueryEntity> descriptor)
        {
            descriptor.Ignore(x => x.CompanyId);
        }
    }

    public record CompanyFindByQueryEntity {
        public Guid? CompanyId {get; init;}
        public string? Slug {get; init;}
    }

    public partial class _Queries {
        [UseDbContext(typeof(AppDbContext))]
        [UsePaging]
        [UseProjection]
        [UseFiltering(typeof(CompanyFindByQueryFilterType))]
        [UseSorting]
        public IQueryable<CompanyFindByQueryEntity> CompanyFindByQuery([ScopedService] AppDbContext db) => db.Companies.Select( x => new CompanyFindByQueryEntity {
            CompanyId = x.CompanyId,
            Slug = x.Slug
        });
    }
}