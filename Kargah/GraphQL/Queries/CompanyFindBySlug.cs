using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using Kargah.Database;
using Kargah.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Queries {

    public partial class _Queries {

        [UseDbContext(typeof(AppDbContext))]
        public Task<Company?> CompanyFindBySlug([ScopedService] AppDbContext db, string slug) {
            return (
                from c in db.Companies
                where c.Slug == slug
                select c
            ).FirstOrDefaultAsync();
        }
    }
}