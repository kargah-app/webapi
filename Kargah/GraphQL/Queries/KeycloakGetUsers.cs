using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Keycloak;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Queries {

    public partial class _Queries {
        [UseDbContext(typeof(AppDbContext))]
        public async Task<IEnumerable<UserRepresentation>> KeycloakGetUsers(
            [Service] IKeycloakRest kc,
            bool? briefRepresentation = null,
            string? email = null,
            int? first = null,
            string? firstName = null,
            string? lastName = null,
            int? max = null,
            string? search = null,
            string? username = null
        ) {
            return await kc.GetUsersAsync(
                briefRepresentation,
                email,
                first,
                firstName,
                lastName,
                max,
                search,
                username
            );
        }
    }
}