using System.Linq;
using HotChocolate;
using Kargah.Database;
using HotChocolate.Types;
using Kargah.Database.Models;
using HotChocolate.Data;

namespace Kargah.GraphQL.Queries {
    public partial class _Queries {
        [UseDbContext(typeof(AppDbContext))]
        [UsePaging]
        public IQueryable<SpentTime> SpentTimeFind([ScopedService]AppDbContext db) => db.SpentTimes;
    }
}