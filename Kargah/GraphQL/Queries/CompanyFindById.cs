using System;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Database.Models;
using Kargah.Keycloak;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Queries {
    public partial class _Queries {
        [UseDbContext(typeof(AppDbContext))]
        public Task<Company?> CompanyFindById(
            [ScopedService] AppDbContext db,
            [Service] IKeycloakAccessToken keycloakAccessToken,
            Guid id
        ) {
            return (
                from c in db.Companies
                where c.CompanyId == id
                select c
            ).FirstOrDefaultAsync();
        }
    }
}