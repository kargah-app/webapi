using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Keycloak;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Mutations {

    public partial class _Mutations {
        public async Task<bool> KeycloakAddUsersRealmRoleMapping(
            [Service] IKeycloakRest kc,
            string userId,
            IEnumerable<RoleRepresentation> roles
        ) {
            return await kc.AddUsersRealmRoleMappingAsync(userId, roles);
        }
    }
}