using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Database.Models;

namespace Kargah.GraphQL.Mutations {

    public record JobCreateInput(
        Guid? companyId,
        string? title
    );

    public class JobCreateInputValidator : AbstractValidator<JobCreateInput> {
        public JobCreateInputValidator() {
            RuleFor(x => x.companyId).NotNull();
            RuleFor(x => x.title).NotEmpty();
        }
    }

    public partial class _Mutations {

        [UseDbContext(typeof(AppDbContext))]
        public async Task<bool?> ValidateJobCreate([ScopedService] AppDbContext db, JobCreateInput input) {
            await new JobCreateInputValidator().ValidateAndThrowAsync(input);
            return true;
        }

        [UseDbContext(typeof(AppDbContext))]
        public async Task<Job?> JobCreate([ScopedService] AppDbContext db, JobCreateInput input) {
            await ValidateJobCreate(db, input);

            var job = new Job {
                JobId = new Guid(),
                CompanyId = input.companyId,
                Title = input.title ?? ""
            };
            db.Jobs.Add(job);
            await db.SaveChangesAsync();
            
            return job;
        }
    }
}