using System;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Database.Models;
using Kargah.GraphQL.Queries;
using Kargah.Keycloak;
using Microsoft.AspNetCore.Http;

namespace Kargah.GraphQL.Mutations {

    public record SpentTimeCreateInput(
        Guid userId,
        long duration = -1,
        long startedAt = -1,
        string description = ""
    );

    public class SpentTimeCreateInputValidator : AbstractValidator<SpentTimeCreateInput> {
        public SpentTimeCreateInputValidator() {}
    }

    public partial class _Mutations {
        [UseDbContext(typeof(AppDbContext))]
        public async Task<SpentTime?> SpentTimeCreate(
            // [Service] IHttpContextAccessor httpContextAccessor,
            [ScopedService]AppDbContext db,
            SpentTimeCreateInput input
        ) {
            // var permissions = KeycloakAuthzGetResoourceScopes()
            // if(
            //     permissions.Any(x => x== "full") || 
            //     (permissions.Any(x => x== "mine") /* And check userId */)
            // ) {
                await new SpentTimeCreateInputValidator().ValidateAndThrowAsync(input);

                var spentTime = new SpentTime() {
                    SpentTimeId = new Guid(),
                    Duration = input.duration,
                    StartedAt = input.startedAt,
                    Description = input.description,
                    UserId = input.userId
                };
                db.SpentTimes.Add(spentTime);
                await db.SaveChangesAsync();

                return spentTime;
            // } else {
            //     throw new UnauthorizedAccessException();
            // }
        }
    }
}