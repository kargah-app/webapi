using System;
using System.Threading.Tasks;

namespace Kargah.GraphQL.Mutations {
    public partial class _Mutations {
        /// <summary>
        /// Execute a test mutation.
        /// </summary>
        /// <param name="input">input value</param>
        /// <returns>Error if input is 'error'</returns>
        public async Task<string?> Test (string input) {
            System.Console.WriteLine($"1: {input}");
            await Task.Delay(1000);
            System.Console.WriteLine($"2: {input}");
            return $"response: {input}";
        }
    }
}