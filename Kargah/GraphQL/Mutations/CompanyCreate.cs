using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Database.Models;
using Kargah.Keycloak;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Mutations {

    public record CompanyCreateInput(
        string slug
    );

    public class CompanyCreateInputValidator : AbstractValidator<CompanyCreateInput> {
        public CompanyCreateInputValidator(AppDbContext db) {
            RuleFor(x => x.slug)
                .NotNull()
                .MinimumLength(4)
                .MustAsync(async (x, cancellationToken) => {
                    var hasSameSlug = await db.Companies.AnyAsync(y => y.Slug == x, cancellationToken);
                    return !hasSameSlug;
                });
        }
    }

    public partial class _Mutations {
        // [UseDbContext(typeof(AppDbContext))]
        // public async Task<Company?> CompanyCreate(
        //     [ScopedService] AppDbContext db,
        //     [Service] IKeycloakAuthz authz,
        //     CompanyCreateInput input
        // ) {
        //     await authz.GetResourcePermissionsAsync("companyCreate");
        //     await new CompanyCreateInputValidator(db).ValidateAndThrowAsync(input);

        //     var company = new Company {
        //         CompanyId = new Guid(),
        //         Slug = input.slug
        //     };
        //     db.Companies.Add(company);
        //     await db.SaveChangesAsync();

        //     return company;
        // }
    }
}