using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using HotChocolate;
using HotChocolate.Data;
using Kargah.Database;
using Kargah.Keycloak;
using Microsoft.EntityFrameworkCore;

namespace Kargah.GraphQL.Mutations {

    public record UserCreateInput(
        Guid companyId,
        string email
    );

    public class UserCreateInputValidator : AbstractValidator<UserCreateInput> {
        public UserCreateInputValidator(
            AppDbContext db,
            IKeycloakRest kc
        ) {
            RuleFor(x => x.companyId)
                .NotNull()
                .MustAsync(async (x, cancellationToken) => {
                    return await db.Companies.AnyAsync(y => y.CompanyId == x, cancellationToken);
                });

            RuleFor(x => x.email)
                .NotNull()
                .EmailAddress()
                .MustAsync(async (x, cancellationToken) => {
                    var users = await kc.GetUsersAsync(email: x);
                    return users.Count() == 0;
                });
        }
    }

    public partial class _Mutations {
        [UseDbContext(typeof(AppDbContext))]
        public async Task<bool?> UserCreate(
            [ScopedService] AppDbContext db,
            [Service] IKeycloakRest kc,
            UserCreateInput input
        ) {
            await new UserCreateInputValidator(db, kc).ValidateAndThrowAsync(input);

            var result = await kc.CreateUserAsync(new UserRepresentation {
                email = input.email,
                username = input.email,
                enabled = true
            });
                
            if(result) {
                var users = await kc.GetUsersAsync(email: input.email);
                var newUser = users.First<UserRepresentation>();
                db.Users.Add(new Database.Models.User {
                    UserId = Guid.Parse(newUser?.id ?? ""),
                    CompanyId = input.companyId
                });
                await db.SaveChangesAsync();
                await kc.VerifyUserEmailAddressAsync(newUser?.id ?? "");
                return true;
            } else {
                throw new Exception("User creation failed at keycloak...");
            }
        }
    }
}