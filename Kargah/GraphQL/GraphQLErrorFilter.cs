using System;
using System.Linq;
using FluentValidation;
using HotChocolate;

namespace Kargah.GraphQL {
    public class GraphQLErrorFilter:IErrorFilter {
        public IError OnError(IError error) {
            if(error.Exception is ValidationException) {
                var ex = (ValidationException) error.Exception;

                var validationError = ErrorBuilder
                    .FromError(error)
                    .SetCode("ValidationException");

                foreach(var fieldError in ex.Errors) {
                    validationError.SetExtension(fieldError.PropertyName, fieldError.ErrorMessage);
                }

                return validationError.Build();
            }
            return error.WithMessage(error.Exception?.Message ?? "Unknown Error");
        }
    }
}