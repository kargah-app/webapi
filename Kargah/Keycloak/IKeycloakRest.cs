using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kargah.Keycloak {
    // https://www.keycloak.org/docs-api/11.0/rest-api/index.html#_overview
    public interface IKeycloakRest {
        Task<int> VerifyUserEmailAddressAsync(
            string userId,
            string? clientId = null,
            string? redirectUri = null
        );

        Task<IEnumerable<UserRepresentation>> GetUsersAsync(
            bool? briefRepresentation = null,
            string? email = null,
            int? first = null,
			string? firstName = null,
            string? lastName = null,
            int? max = null,
            string? search = null,
            string? username = null
        );

        Task<bool> CreateUserAsync(
            UserRepresentation user
        );

        Task<IEnumerable<RoleRepresentation>> GetUsersRealmRoleMappingAsync(
            string userId
        );

        Task<bool> AddUsersRealmRoleMappingAsync(
            string userId,
            IEnumerable<RoleRepresentation> roles
        );

        Task<IEnumerable<RoleRepresentation>> GetAllRolesAsync();
    }

    public record RoleRepresentation { // Not complete
        public string? id { get; init; }
        public string? name { get; init; }
    }

    public record UserRepresentation {
        public IDictionary<string, List<string>>? attributes { get; init; }
        public IDictionary<string, object>? clientRoles { get; init; }
        public long? createdTimestamp { get; init; }
        public ReadOnlyCollection<string>? DisableableCredentialTypes { get; init; }
        public string? email { get; init; }
        public bool? emailVerified { get; init; }
        public bool? enabled { get; init; }
        public string? federationLink { get; init; }
        public string? firstName { get; init; }
        public List<string>? groups { get; init; }
        public string? lastName { get; init; }
        public string? id { get; init; }
        public string? origin { get; init; }
        public List<string>? realmRoles { get; init; }
        public List<string>? requiredActions { get; init; }
        public string? username { get; init; }
    };
}