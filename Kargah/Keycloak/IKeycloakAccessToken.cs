namespace Kargah.Keycloak {
    // https://www.keycloak.org/docs-api/11.0/rest-api/index.html#_overview
    public interface IKeycloakAccessToken {
        public string[]? GetResourceScopes(string resourceName);
    }
}