using System.Collections.Generic;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Flurl.Http.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Kargah.Keycloak {
    public class KeycloakRest : IKeycloakRest {
        public string tokenUrl {get; init;} = "";
        public List<KeyValuePair<string, string>>? tokenParameters {get; init;} = null;
        public string restUrl {get; init;} = "";

        public string? accessToken {get; private set; } = null;
        public string? refreshToken {get; private set; } = null;

        private readonly ISerializer _serializer = new NewtonsoftJsonSerializer(
            new JsonSerializerSettings {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
            }
        );

        private async Task<string> GetAccessToken() {
            var result = await tokenUrl
                .WithHeader("Accept", "application/json")
                .PostUrlEncodedAsync(tokenParameters)
                .ReceiveJson()
                .ConfigureAwait(false);

            return result.access_token.ToString();
        }

        private async Task<IFlurlRequest> GetUrl() {
            // restUrl = $"{C["keycloak:kargah:host"]}/auth/admin/realms/{C["keycloak:kargah:realm"]}",

            var token = await GetAccessToken();
            return new Url(restUrl)
                // .AppendPathSegment("/auth")
                .ConfigureRequest(settings => settings.JsonSerializer = _serializer)
                .WithOAuthBearerToken(token);
        }

        public async Task<int> VerifyUserEmailAddressAsync(
            string userId,
            string? clientId = null,
            string? redirectUri = null
        ) {
			var response = await (await GetUrl())
				.AppendPathSegment($"/users/{userId}/send-verify-email")
				.SetQueryParams(new Dictionary<string, object?> {
                    ["client_id"] = clientId,
                    ["redirect_uri"] = redirectUri
                })
				.PutAsync();
			return response.StatusCode;
		}

        public async Task<IEnumerable<UserRepresentation>> GetUsersAsync(
            bool? briefRepresentation = null,
            string? email = null,
            int? first = null,
			string? firstName = null,
            string? lastName = null,
            int? max = null,
            string? search = null,
            string? username = null
        ) {
			return await (await GetUrl())
				.AppendPathSegment($"/users")
				.SetQueryParams(new Dictionary<string, object?> {
                    [nameof(briefRepresentation)] = briefRepresentation,
                    [nameof(email)] = email,
                    [nameof(first)] = first,
                    [nameof(firstName)] = firstName,
                    [nameof(lastName)] = lastName,
                    [nameof(max)] = max,
                    [nameof(search)] = search,
                    [nameof(username)] = username
                })
				.GetJsonAsync<IEnumerable<UserRepresentation>>()
				.ConfigureAwait(false);
		}

        public async Task<bool> CreateUserAsync(UserRepresentation user) {
			var response = await (await GetUrl())
			    .AppendPathSegment($"/users")
			    .PostJsonAsync(user);
			return response.StatusCode < 300;
		}

        public async Task<IEnumerable<RoleRepresentation>> GetUsersRealmRoleMappingAsync(string userId) {
            return await (await GetUrl())
			    .AppendPathSegment($"/users/{userId}/role-mappings/realm")
			    .GetJsonAsync<IEnumerable<RoleRepresentation>>()
				.ConfigureAwait(false);
        }

        public async Task<bool> AddUsersRealmRoleMappingAsync(string userId, IEnumerable<RoleRepresentation> roles) {
            var response = await (await GetUrl())
			    .AppendPathSegment($"/users/{userId}/role-mappings/realm")
			    .PostJsonAsync(roles);
			return response.StatusCode < 300;
        }

        public async Task<IEnumerable<RoleRepresentation>> GetAllRolesAsync() {
            return await (await GetUrl())
			    .AppendPathSegment($"/roles")
			    .GetJsonAsync<IEnumerable<RoleRepresentation>>()
				.ConfigureAwait(false);
        }
    }
}