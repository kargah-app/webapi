using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Linq;

namespace Kargah.Keycloak {
    public record AuthorizationPermissions (
        string[] scopes,
        string rsname
    );

    public record AuthorizationClaim (
        AuthorizationPermissions[] permissions
    );

    public class KeycloakAccessToken : IKeycloakAccessToken {
        private IHttpContextAccessor _httpContextAccessor;

        public KeycloakAccessToken(IHttpContextAccessor httpContextAccessor) {
            this._httpContextAccessor = httpContextAccessor;
        }

        public string[]? GetResourceScopes(string resourceName) {
            var identity = _httpContextAccessor.HttpContext?.User?.Identity;

            if(identity != null) {
                var claimsIdentity = (ClaimsIdentity)identity;
                var authorizationClaimValue = claimsIdentity.FindFirst(claim => claim.Type == "authorization")?.Value;

                if(authorizationClaimValue != null) {
                    var authorization = JsonConvert.DeserializeObject<AuthorizationClaim>(authorizationClaimValue);

                    return authorization.permissions.FirstOrDefault(x => x.rsname == resourceName)?.scopes;
                }
            }

            return null;
        }
    }
}