namespace Kargah.Utils {
    public record AppSettings (
        Keycloak keycloak
    );

    public record Keycloak (
        KeycloakRealm kargah
    );

    public record KeycloakRealm (
        string host,
        string realm,
        string client_id,
        string client_secret
    );
}