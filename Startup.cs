using Kargah.Database;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using HotChocolate;
using Kargah.GraphQL.Queries;
using Kargah.GraphQL.Mutations;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Kargah.Keycloak;
using Kargah.GraphQL;
using HotChocolate.Types;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Protocols;
using Kargah.Utils;
using Microsoft.AspNetCore.Authentication;

namespace Kargah
{
    public class Startup
    {
        public IConfiguration C { get; }

        public Startup(IConfiguration configuration)
        {
            C = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddCors(options => {
                options.AddDefaultPolicy(builder => {
                    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                });
            });

            services.AddHttpContextAccessor();
            // services.AddTransient<IClaimsTransformation, ClaimsTransformer>();
            services.AddTransient<IKeycloakAccessToken, KeycloakAccessToken>();

            services
                .AddAuthentication(options => {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o => {
                    o.Authority = $"{C["keycloak:kargah:host"]}/auth/realms/{C["keycloak:kargah:realm"]}";
                    o.Audience = "webapi";
                    o.RequireHttpsMetadata = false;
                    // o.TokenValidationParameters.NameClaimType = "preferred_username";
                    // o.TokenValidationParameters.RoleClaimType = "role";

                    var cm = new ConfigurationManager<OpenIdConnectConfiguration>(
                        $"{C["keycloak:kargah:host"]}/auth/realms/{C["keycloak:kargah:realm"]}/.well-known/openid-configuration",
                        new OpenIdConnectConfigurationRetriever(),
                        new HttpDocumentRetriever() {
                            RequireHttps = false
                        }
                    );

                    o.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters() {
                        RequireExpirationTime = true,
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKeys = cm.GetConfigurationAsync().Result.SigningKeys
                    };

                    o.Events = new JwtBearerEvents {
                        OnAuthenticationFailed = c => {
                            System.Console.WriteLine(c.Exception.Message);
                            return Task.Delay(0);
                        }
                    };
                });

                //https://www.youtube.com/watch?v=kLwjIqNg0Mc&ab_channel=Thinktecture
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            /*
            app.Use(async (context, next) => {
                var jwtOrigins = context.User.FindAll(x => {
                    return x.Type == "allowed-origins";
                }).Select(x => x.Value).ToArray();

                context.Request.Headers.TryGetValue("Origin", out StringValues requestOrigin);

                var corsService = context.RequestServices.GetService<ICorsService>();

                if(corsService != null) {
                    var corsPolicy = new CorsPolicyBuilder()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .WithOrigins(jwtOrigins.Length > 0 ? jwtOrigins : new string[] {requestOrigin})
                        .Build();

                    var corsResult = corsService.EvaluatePolicy(context, corsPolicy);
                    corsService.ApplyResult(corsResult, context.Response);

                    if(corsResult.IsPreflightRequest) {
                        Console.WriteLine("preflight");
                        context.Response.StatusCode = StatusCodes.Status204NoContent;
                        await context.Response.CompleteAsync();
                    } else {
                        Console.WriteLine("not preflight");
                        await next.Invoke();
                    }
                }
            });
            */
            app.UseCors();
            app.UseRouting();
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapGraphQL();
                endpoints.MapControllers();
                endpoints.MapGet("/", context => {
                    context.Response.Redirect("/graphql");
                    return Task.CompletedTask;
                });
            });
        }
    }
}
